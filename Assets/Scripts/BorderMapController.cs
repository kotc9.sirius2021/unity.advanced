﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * github/gitlab/telegram - @kotC9
 * Sokolov Sergey
 */
public class BorderMapController : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        var objectName = other.gameObject.name;
        Debug.Log($"{objectName} left from map");

        if (objectName == "unitychan")
        {
            SceneManager.LoadScene("GameOver");
            Destroy(other.gameObject);
        }
    }
}
