﻿
    using UnityEngine;

    public class UnityChanController : MonoBehaviour
    {
        public float animSpeed = 1.5f;				
        public float lookSmoother = 3.0f;
        public bool useCurves = true;
        public float useCurvesHeight = 0.5f;
        public float forwardSpeed = 7.0f;
        public float backwardSpeed = 2.0f;
        public float rotateSpeed = 2.0f;
        public float jumpPower = 3.0f; 
        
        
    }