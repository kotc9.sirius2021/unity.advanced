﻿//
// Unityちゃん用の三人称カメラ
// 
// 2013/06/07 N.Kobyasahi
//
using UnityEngine;
using System.Collections;

namespace UnityChan
{
	public class ThirdPersonCamera : MonoBehaviour
	{
		private Transform eyePos;	
	
	
		void Start ()
		{
			eyePos= GameObject.Find ("EyePos").transform;
			transform.position = eyePos.position;	
			transform.forward = eyePos.forward;	
		}
	
		void FixedUpdate ()	// このカメラ切り替えはFixedUpdate()内でないと正常に動かない
		{
		
				SetCameraPositionNormalView ();
		}

		void SetCameraPositionNormalView ()
		{
			transform.position = eyePos.position;	
			transform.forward = eyePos.forward;
		}
	}
}